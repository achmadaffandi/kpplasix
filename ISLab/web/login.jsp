<%-- 
    Document   : login
    Created on : Dec 27, 2015, 2:07:31 PM
    Author     : Hendra
--%>
        <div class="page-content">
            <div class="padding20 block-shadow fg-blue" style="margin:80px 400px">
                <form id="myForm" method=post>
                    <h1 class="text-light">Login | LAB SI ITS</h1>
                    <hr class="thin"/>
                    <br />
                    <div class="input-control text full-size" data-role="input">
                        <label>Username</label>
                        <input type="text" id="id" name="id" maxlength="10" class="required">
                    </div>
                    <br />
                    <br />
                    <div class="input-control password full-size" data-role="input">
                        <label>Password</label>
                        <input type="password" name=password class="required">
                    </div>
                    <br />
                    <br />
                    <div class="form-actions">
                        <button type="submit" class="button primary"> Login </button>
                    </div>
                    <br />
                    <div class="input-control password full-size" data-role="input">
                        <label>Not have any account? Sign Up <a href="./signup.jsp">here</a></label>
                    </div>
                </form>
            </div>
        </div>
