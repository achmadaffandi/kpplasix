
        <div class="page-content">
		<div class="formback padding20 block-shadow fg-blue" style="margin:20px 300px">
			<form method="post" action="save_daftar.jsp" name="formdaftar" id="frm">
				<h1 class="text-light">Daftar Member Laboratorium</h1>
				<hr class="thin"/>
				<br />
				<h4 class="text-light">Lengkapi semua data berikut</h4>
				<hr class="thin"/>
				<br />
				<div class="input-control text" data-role="input" style="width: 59%">
					<label>NRP/NIP</label>
                                        <input type="text" name="nrpnip" maxlength="10" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Nama Lengkap</label>
					<input type="text" name="nama" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Jenis Kelamin</label>
					<select name="gender">
                                            <option value="male" selected="selected">Laki-laki </option>
                                            <option value="female">Perempuan</option>
                                        </select>
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Email</label>
					<input type="email" name="emaul" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Nomor Telepon/HP</label>
					<input type="text" name="phone" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Username</label>
					<input type="text" name="username" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input">
					<label>Password</label>
					<input type="password" name="password" class="required">
				</div>
                                <div class="input-control text" data-role="input">
					<label>Re-enter Password</label>
					<input type="password" name="repassword" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Group</label>
					<select name="group">
                                            <option value="dosen" selected="selected">Dosen</option>
                                            <option value="karyawan">Karyawan</option>
                                            <option value="mahasiswa">Mahasiswa</option>
                                        </select>
				</div>
				<br />
				<br />
				<div class="form-actions">
					<button type="submit" class="button primary"> Submit </button>
				</div>
			</form>
		</div>
	</div>