<%-- 
    Document   : userui
    Created on : Jan 1, 2016, 6:47:12 PM
    Author     : user
--%>

<html>
    <head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Kerja Praktek Sistem Informasi ITS">
    <meta name="keywords" content="KP, Kerja Praktek, Sistem Informasi, ITS">
    <meta name="author" content="Hendra Rismana">

    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
    <title>Laboratorium Sistem Informasi ITS</title>

    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myForm").validate({
                rules: {
                    id : {
                        digits: true,
                        minlength:10,
                        maxlength:10
                    },
                }
            });
        });
    </script>

    </head>
    <body>
        <div>
            <div class="app-bar bg-blue fixed-top" data-role="appbar">
		<a class="app-bar-element" href="./">LABORATORIUM SI ITS</a>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./signup.jsp"><span class="mif-enter"></span> LogOut</a>
                </div>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./editprofile.jsp"><span class="mif-enter"></span> Profil</a>
                </div>
		<div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./contactus.jsp"> CONTACT US </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./peminjaman.jsp"> PEMINJAMAN </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./index.jsp"> HOME </a></li>
                    </ul>
                </div>
            </div>
        </div>

<div class="page-content">
    <div class="formback padding20 block-shadow fg-blue" style="margin:30px 400px">
        <form id="myForm" method=post action=submit>
            <h1 class="text-light">Peminjaman | LAB SI ITS</h1>
            <hr class="thin"/>
            <br />
            <div class="element-select">
                <label class="title"><span class="required">Asset yang Dipinjam</span></label>
                <p><span>
                        <select name="assetdipinjam" required="required">
                            <option value="">Pilih Item</option>
                            <option value=""></option>
                        </select>
                    </span>      </p>
            </div>
            <br />
            <div class="item text">
                <label class="title">
                    <span class="required">
                        Tanggal Peminjaman
                    </span>
                </label> 
                <p>
                    <input class="large" data-format="yyyy-mm-dd" type="date" name="tanggalpeminjaman" required="required" placeholder="Tanggal Peminjaman" />
                </p>
            </div>
            <br />
            <div class="item text">
                <label class="title">
                    <span class="required">
                        Tanggal Kembali
                    </span>
                </label>
                <p>
                    <input class="large" data-format="yyyy-mm-dd" type="date" name="tanggalkembali" required="required" placeholder="Tanggal Kembali" />
                </p>
            </div>
            <br/>
            <div class="form-actions" align="center">
		<button type="submit" class="button primary"> Submit </button>
            </div>
            <hr>
        </form>
    </div>
</div>
        
        
        
        <div id="footer">
    <div class="bg-blue fg-white">
        <div class="container">
            <div class="align-center padding20 text-medium">
                LABORATORIUM | SISTEM INFORMASI ITS
            </div>
        </div>
    </div>
</div>
</body>
</html>