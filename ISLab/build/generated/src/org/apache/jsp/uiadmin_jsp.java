package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class uiadmin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("\n");
      out.write("      \n");
      out.write("      <div style=\"width: 95%; margin: 0 auto;\" align = \"center\">\n");
      out.write("\n");
      out.write("        \n");
      out.write("        <h2>Hallo Admin</h2>\n");
      out.write("<div class=\"container\" align = \"center\">\n");
      out.write("    <h1>Hallo Admin</h1>\n");
      out.write("       \n");
      out.write("            <form >\n");
      out.write("                <h2>Berikut adalah Daftar Peminjaman</h2>\n");
      out.write("            </form>\n");
      out.write("         <br>\n");
      out.write("         <div class=\"panel panel-primary\">\n");
      out.write("            \n");
      out.write("                <div class=\"panel-body\">\n");
      out.write("                 <table border = 1 align = \"center\" class=\"table table-hover table-striped\">\n");
      out.write("                    <thead>\n");
      out.write("                       <tr>\n");
      out.write("\t\t\t\t\t   \n");
      out.write("\t\t\t\t\t   <td style=\"height:40px;\"><b>No</b></td> \n");
      out.write("\t\t\t\t\t   \n");
      out.write("                          <td ><b>Kode Asset</b></td>\n");
      out.write("                         \n");
      out.write("                       \n");
      out.write("                          <td style=\"height:20px;\"><b>Nama Barang</b></td>\n");
      out.write("                          <td ><b>Time<b></td>\n");
      out.write("                          \n");
      out.write("                          <td ><b>Peminjam</b></td>\n");
      out.write("                          <td ><b>Approval</b></td>\n");
      out.write("                          \n");
      out.write("                       </tr>\n");
      out.write("                    </thead>\n");
      out.write("                    <tbody>\n");
      out.write("                       \n");
      out.write("                       <tr>\n");
      out.write("                          <td>1</td>\n");
      out.write("                          <td>AST001</td>\n");
      out.write("                          <td>Laptop</td>\n");
      out.write("                          <td>31/12/2015</td>\n");
      out.write("                          <td>Arsita Wardani</td>\n");
      out.write("                        <td><select name=\"nama_aset\">\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Terima\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Tolak\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Sudah Kembali\n");
      out.write("                        </option>\n");
      out.write("                      </select>\n");
      out.write("                        \n");
      out.write("                        </td>\n");
      out.write("                          \n");
      out.write("            </td>\n");
      out.write("                          \n");
      out.write("                        </tr>\n");
      out.write("                        \n");
      out.write("                        <tr>\n");
      out.write("                          <td>2</td>\n");
      out.write("                          <td>AST002</td>\n");
      out.write("                          <td>Laptop</td>\n");
      out.write("                          <td>06/12/2015</td>\n");
      out.write("                          <td>Andrian P</td>\n");
      out.write("                          \n");
      out.write("                          <td><select name=\"nama_aset\">\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Terima\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Tolak\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Sudah Kembali\n");
      out.write("                        </option> </td>\n");
      out.write("                      </select>\n");
      out.write("\n");
      out.write("                        </tr>\n");
      out.write("                         <tr>\n");
      out.write("                          <td>3</td>\n");
      out.write("                          <td>AST003</td>\n");
      out.write("                          <td>LCD Proyektor</td>\n");
      out.write("                          <td>12/12/2015</td>\n");
      out.write("                          <td>Muh. Alvin Nadhul</td>\n");
      out.write("                        <td><select name=\"nama_aset\">\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Terima\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Tolak\n");
      out.write("                        </option>\n");
      out.write("                        <option value=\"\" selected=\"selected\">\n");
      out.write("                          Sudah Kembali\n");
      out.write("                        </option>\n");
      out.write("                      </select>\n");
      out.write("                        \n");
      out.write("                        </td>\n");
      out.write("                          \n");
      out.write("          \n");
      out.write("                          \n");
      out.write("                        </tr>\n");
      out.write("                      \n");
      out.write("                          \n");
      out.write("                        </tr>\n");
      out.write("                    </tbody>\n");
      out.write("                 </table>\n");
      out.write("                    \n");
      out.write("                   <button type=\"submit\" class=\"button primary\"> SAVE </button>\n");
      out.write("                   <p></p>\n");
      out.write("                   <p></p>\n");
      out.write("                   <p></p>\n");
      out.write("                   <p></p>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("         \n");
      out.write("            </div>\n");
      out.write("  \n");
      out.write("     \n");
      out.write("\n");
      out.write("\n");
      out.write("  </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
