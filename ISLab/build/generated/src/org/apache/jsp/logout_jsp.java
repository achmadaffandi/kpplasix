package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class logout_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/WEB-INF/jspf/header.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/footer.jspf");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\r\n");
      out.write("    <head lang=\"en\">\r\n");
      out.write("    <meta charset=\"UTF-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\r\n");
      out.write("    <meta name=\"description\" content=\"Kerja Praktek Sistem Informasi ITS\">\r\n");
      out.write("    <meta name=\"keywords\" content=\"KP, Kerja Praktek, Sistem Informasi, ITS\">\r\n");
      out.write("    <meta name=\"author\" content=\"Hendra Rismana\">\r\n");
      out.write("\r\n");
      out.write("    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />\r\n");
      out.write("    <title>Laboratorium Sistem Informasi ITS</title>\r\n");
      out.write("\r\n");
      out.write("    <link href=\"css/metro.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"css/metro-icons.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"css/docs.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.validate.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        $(document).ready(function() {\r\n");
      out.write("            $(\"#myForm\").validate({\r\n");
      out.write("                rules: {\r\n");
      out.write("                    id : {\r\n");
      out.write("                        digits: true,\r\n");
      out.write("                        minlength:10,\r\n");
      out.write("                        maxlength:10\r\n");
      out.write("                    },\r\n");
      out.write("                }\r\n");
      out.write("            });\r\n");
      out.write("        });\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div>\r\n");
      out.write("            <div class=\"app-bar bg-blue fixed-top\" data-role=\"appbar\">\r\n");
      out.write("\t\t<a class=\"app-bar-element\" href=\"./\">LABORATORIUM SI ITS</a>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <a class=\"fg-white\" href=\"./signup.jsp\"><span class=\"mif-enter\"></span> SIGN UP</a>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <a class=\"fg-white\" href=\"./login.jsp\"><span class=\"mif-enter\"></span> LOGIN</a>\r\n");
      out.write("                </div>\r\n");
      out.write("\t\t<div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./contactus.jsp\"> CONTACT US </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./peminjaman.jsp\"> PEMINJAMAN </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./index.jsp\"> HOME </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>");
      out.write("\n");
      out.write("\n");
      out.write("<?php\n");
      out.write("session_start();\n");
      out.write("unset( $_SESSION['user'] );\n");
      out.write("?>\n");
      out.write("<h2>Anda telah berhasil logout..</h2>\n");
      out.write("Silahkan klik <a href=\"login.jsp\"><b>disini</b></a> untuk login kembali");
      out.write("<div id=\"footer\">\r\n");
      out.write("    <div class=\"bg-blue fg-white\">\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"align-center padding20 text-medium\">\r\n");
      out.write("                LABORATORIUM | SISTEM INFORMASI ITS\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
