package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class contactus_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/WEB-INF/jspf/header.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/footer.jspf");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>\r\n");
      out.write("    <head lang=\"en\">\r\n");
      out.write("    <meta charset=\"UTF-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\r\n");
      out.write("    <meta name=\"description\" content=\"Kerja Praktek Sistem Informasi ITS\">\r\n");
      out.write("    <meta name=\"keywords\" content=\"KP, Kerja Praktek, Sistem Informasi, ITS\">\r\n");
      out.write("    <meta name=\"author\" content=\"Hendra Rismana\">\r\n");
      out.write("\r\n");
      out.write("    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />\r\n");
      out.write("    <title>Laboratorium Sistem Informasi ITS</title>\r\n");
      out.write("\r\n");
      out.write("    <link href=\"css/metro.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"css/metro-icons.css\" rel=\"stylesheet\">\r\n");
      out.write("    <link href=\"css/docs.css\" rel=\"stylesheet\">\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.validate.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("        $(document).ready(function() {\r\n");
      out.write("            $(\"#myForm\").validate({\r\n");
      out.write("                rules: {\r\n");
      out.write("                    id : {\r\n");
      out.write("                        digits: true,\r\n");
      out.write("                        minlength:10,\r\n");
      out.write("                        maxlength:10\r\n");
      out.write("                    },\r\n");
      out.write("                }\r\n");
      out.write("            });\r\n");
      out.write("        });\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div>\r\n");
      out.write("            <div class=\"app-bar bg-blue fixed-top\" data-role=\"appbar\">\r\n");
      out.write("\t\t<a class=\"app-bar-element\" href=\"./\">LABORATORIUM SI ITS</a>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <a class=\"fg-white\" href=\"./signup.jsp\"><span class=\"mif-enter\"></span> LOGIN</a>\r\n");
      out.write("                </div>\r\n");
      out.write("\t\t<div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./contactus.jsp\"> CONTACT US </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./peminjaman.jsp\"> PEMINJAMAN </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"app-bar-element place-right\">\r\n");
      out.write("                    <ul class=\"app-bar-menu\">\r\n");
      out.write("                        <li><a href=\"./index.jsp\"> HOME </a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>`\r\n");
      out.write("           \r\n");
      out.write("    \r\n");
      out.write("    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\r\n");
      out.write("    <title>CONTACT US</title>\r\n");
      out.write("    <div class=\"page-content\">\r\n");
      out.write("\t\t<div class=\"formback padding20 block-shadow fg-blue\" style=\"margin:20px 100px\">\r\n");
      out.write("                   \r\n");
      out.write("    <div class=\"container\" align = \"center\">\r\n");
      out.write("      <div class=\"panel panel-primary\">\r\n");
      out.write("       \r\n");
      out.write("    <div class=\"section section-breadcrumbs\">\r\n");
      out.write("\t\t\t<div class=\"container\">\r\n");
      out.write("\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t<div class=\"col-md-6\">\r\n");
      out.write("\t\t\t\t\t\t<h2>Contact Us</h2>\r\n");
      out.write("\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("        \r\n");
      out.write("          </div>\r\n");
      out.write("        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("        \r\n");
      out.write("        <div class=\"section\">\r\n");
      out.write("\t    \t<div class=\"container\">\r\n");
      out.write("\t        \t<div class=\"row\">\r\n");
      out.write("\t        \t\t<div class=\"col-sm-7\">\r\n");
      out.write("\t        \t\t\t<!-- Map -->\r\n");
      out.write("\t        \t\t\t<div id=\"contact-us-map\">\r\n");
      out.write("\r\n");
      out.write("\t        \t\t\t</div>\r\n");
      out.write("\t        \t\t\t<!-- End Map -->\r\n");
      out.write("\t        \t\t\t<!-- Contact Info -->\r\n");
      out.write("\t        \t\t\t<p class=\"contact-us-details\">\r\n");
      out.write("\t        \t\t\t\t<b>Address:</b>\r\n");
      out.write("                                        <p>\r\n");
      out.write("                                            Jurusan Sistem Informasi, Kampus SUKOLILO </p>\r\n");
      out.write("                                        <p> Institut Teknologi Sepuluh Nopember </p>\r\n");
      out.write("                                        <p> Surabaya</p>\r\n");
      out.write("                                        <p>\r\n");
      out.write("                                                <b>Phone:</b> +6281 654321<br/>\r\n");
      out.write("                                        </p>\r\n");
      out.write("\t        \t\t\t\t<b>Fax:</b> +654321<br/>\r\n");
      out.write("                                                <p>\r\n");
      out.write("\t        \t\t\t\t<b>Website:</b> <a href=\"si.its.ac.id\">si.its.ac.id</a>\r\n");
      out.write("                                                </p>\r\n");
      out.write("                                                \r\n");
      out.write("                                                \r\n");
      out.write("                                                \r\n");
      out.write("\t        \t\t\t</p>\r\n");
      out.write("\t        \t\t\t<!-- End Contact Info -->\r\n");
      out.write("\t        \t\t</div>\r\n");
      out.write("\t        \t\t<div class=\"col-sm-5\">\r\n");
      out.write("\t        \t\t\t<!-- Contact Form -->\r\n");
      out.write("                                        \r\n");
      out.write("\t        \t\t\t<h3>Send Us a Message</h3>\r\n");
      out.write("\t        \t\t\t<div class=\"contact-form-wrapper\">\r\n");
      out.write("\t\t        \t\t\t<form class=\"form-horizontal\" role=\"form\">\r\n");
      out.write("\t\t        \t\t\t\t <div class=\"form-group\">\r\n");
      out.write("\t\t        \t\t\t\t \t<label for=\"Name\" class=\"col-sm-3 control-label\"><b>Your name</b></label>\r\n");
      out.write("\t\t        \t\t\t\t \t<div class=\"col-sm-9\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"Name\" type=\"text\" placeholder=\"\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t\r\n");
      out.write("                                                    <div class=\"form-group\">\r\n");
      out.write("                                                        <label for=\"contact-email\" class=\"col-sm-3 control-label\"><b>Your Email</b></label>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<input class=\"form-control\" id=\"contact-email\" type=\"text\" placeholder=\"\">\r\n");
      out.write("                                                                                </div>\r\n");
      out.write("                                                    \r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("                                                \r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<label for=\"contact-message\" class=\"col-sm-3 control-label\"><b>Select Group</b></label>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"prependedInput\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<option>Pilih Group</option>\r\n");
      out.write("                                                                                        <option>Mahasiswa</option>\r\n");
      out.write("                                                                                        <option>Dosen</option>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<option>Karyawan</option>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</select>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<label for=\"contact-message\" class=\"col-sm-3 control-label\"><b>Message</b></label>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"5\" id=\"contact-message\"></textarea>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn pull-right\">Send</button>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t        \t\t\t</form>\r\n");
      out.write("\t\t        \t\t</div>\r\n");
      out.write("\t\t        \t\t<!-- End Contact Info -->\r\n");
      out.write("\t        \t\t</div>\r\n");
      out.write("\t        \t</div>\r\n");
      out.write("\t    \t</div>\r\n");
      out.write("\t    </div>\r\n");
      out.write("\r\n");
      out.write("\t    \r\n");
      out.write("\t\t    \t\r\n");
      out.write("</head>\r\n");
      out.write("</html>\r\n");
      out.write("<div id=\"footer\">\r\n");
      out.write("    <div class=\"bg-blue fg-white\">\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"align-center padding20 text-medium\">\r\n");
      out.write("                LABORATORIUM | SISTEM INFORMASI ITS\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
