<%-- 
    Document   : peminjaman
    Created on : Dec 27, 2015, 2:08:06 PM
    Author     : Hendra
--%>

<div class="page-content">
    <div class="formback padding20 block-shadow fg-blue" style="margin:80px 400px">
        <form id="myForm" method=post action=submit>
            <h1 class="text-light">Peminjaman | LAB SI ITS</h1>
            <hr class="thin"/>
            <br />
            <div class="element-select">
                <label class="title"><span class="required">Asset yang Dipinjam</span></label>
                <p><span>
                        <select name="assetdipinjam" required="required">
                            <option value="">Pilih Item</option>
                            <option value=""></option>
                        </select>
                    </span>      </p>
            </div>
            <br />
            <div class="item text">
                <label class="title">
                    <span class="required">
                        Tanggal Peminjaman
                    </span>
                </label> 
                <p>
                    <input class="large" data-format="yyyy-mm-dd" type="date" name="tanggalpeminjaman" required="required" placeholder="Tanggal Peminjaman" />
                </p>
            </div>
            <br />
            <div class="item text">
                <label class="title">
                    <span class="required">
                        Tanggal Kembali
                    </span>
                </label>
                <p>
                    <input class="large" data-format="yyyy-mm-dd" type="date" name="tanggalkembali" required="required" placeholder="Tanggal Kembali" />
                </p>
            </div>
            <br/>
            <class="form-line" data-type="control_checkbox" id="id_5">
                <label class="form-label form-label-left form-label-auto" id="label_5" for="input_5">  </label>
                <div id="cid_5" class="form-input jf-required">
                    <div class="form-single-column">
                        <span class="form-checkbox-item" style="clear:left;">
                            <span class="dragger-item">
                            </span>
                            <input type="checkbox" class="form-checkbox" id="input_5_0" name="q5_input5[]" value="Option 1" />
                            <label id="label_input_5_0" for="input_5_0"> Saya menyetujui syarat dan ketentuan yang berlaku </label>
                        </span>
                    </div>
                </div>
                <li style="display:none">
                    Should be Empty:
                    <input type="text" name="website" value="" />
                </li>
                <input type="hidden" id="simple_spc" name="simple_spc" value="53623528590459" />
                <script type="text/javascript">
                    document.getElementById("si" + "mple" + "_spc").value = "53623528590459-53623528590459";
                </script>
                <div class="form-actions" align="center">
                    <button type="submit" class="button primary"> Submit </button>
                </div>
                <hr>
                <br/>
        </form>
    </div>
</div>