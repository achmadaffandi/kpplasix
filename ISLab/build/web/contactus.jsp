<%-- 
    Document   : editprofile
    Created on : Dec 28, 2015, 4:49:43 AM
    Author     : fandi
--%>
<div class="page-content">
    <div class="formback padding20 block-shadow fg-blue" style="margin:20px 300px">
        <form id="myForm" method=post action=?menu=submiteditan>
            <h1 class="text-light" align="center">Contact Us</h1>
            <hr class="thin"/>
            <br />
            <h3 class="text-light">Alamat</h3>
            <h4 class="text-light">Jurusan Sistem Informasi, Kampus Sukolilo
                <br>
                Institut Teknologi Sepuluh Nopember
                <br>
                Surabaya
            </h4>
            <h3 class="text-light">Telepon</h3>
            <h4 class="text-light">+6281 654321</h4>
            <h3 class="text-light">Fax</h3>
            <h4 class="text-light">+654321</h4>
            <h3 class="text-light">Website</h3>
            <a href="is.its.ac.id" class="h4 text-light">www.is.its.ac.id</a>
            <br />
            <br />
            <br />
            <div class="input-control text" data-role="input" style="width: 59%">
                <label>Nama Anda</label>
                <input type="text" name="namakontak" class="required">
            </div>
            <br />
            <br />
            <div class="input-control text" data-role="input" style="width: 59%">
                <label>Alamat Email</label>
                <input type="email" name="emailkontak" class="required">
            </div>
            <br />
            <br />
            <div class="input-control text" data-role="input" style="width: 59%">
                <label>Group</label>
                <select name="groupkontak">
                    <option value="dosenkontak" selected="selected">Dosen</option>
                    <option value="karyawankontak">Karyawan</option>
                    <option value="mahasiswakontak">Mahasiswa</option>
                </select>
            </div>
            <br />
            <br />
            <div class="input-control text" data-role="input" style="width: 59%">
                <label>Pesan</label>
                <textarea type="text" name="pesankontak" class="required"> </textarea>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary"> Kirim </button>
            </div>
        </form>
    </div>
</div>
