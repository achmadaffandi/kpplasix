<html>
    <head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Kerja Praktek Sistem Informasi ITS">
    <meta name="keywords" content="KP, Kerja Praktek, Sistem Informasi, ITS">
    <meta name="author" content="Hendra Rismana">

    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
    <title>Laboratorium Sistem Informasi ITS</title>

    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myForm").validate({
                rules: {
                    id : {
                        digits: true,
                        minlength:10,
                        maxlength:10
                    },
                }
            });
        });
    </script>

    </head>
    <body>
        <div>
            <div class="app-bar bg-blue fixed-top" data-role="appbar">
		<a class="app-bar-element" href="./">LABORATORIUM SI ITS</a>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./logout.jsp"><span class="mif-enter"></span> LogOut</a>
                </div>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./profile2.jsp"><span class="mif-enter"></span> Profil</a>
                </div>
		<div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./contactus.jsp"> CONTACT US </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./peminjaman.jsp"> PEMINJAMAN </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./index.jsp"> HOME </a></li>
                    </ul>
                </div>
            </div>
        </div>
<div class="page-content">
    <div class="formback padding20 block-shadow fg-blue" 
         style="margin:20px 300px;">
        <h1 class="text-light">Profil</h1>
        <hr class="thin"/>
        <br />
        <h4 class="text-light">NRP/NIP</h4>
        <h3 class="text-light">5213100102</h3>
        <h4 class="text-light">Nama Lengkap</h4>
        <h3 class="text-light">Achmad Affandi</h3>
        <h4 class="text-light">Jenis Kelamin</h4>
        <h3 class="text-light">Laki-laki</h3>
        <h4 class="text-light">Email</h4>
        <h3 class="text-light">madaffandi@gmail.com</h3>
        <h4 class="text-light">Nomor Telepon/HP</h4>
        <h3 class="text-light">08999999999</h3>
        <h4 class="text-light">Username</h4>
        <h3 class="text-light">fkaroi</h3>
        <h4 class="text-light">Password</h4>
        <h3 class="text-light">*******</h3>
        <h4 class="text-light">Group</h4>
        <h3 class="text-light">Mahasiswa</h3>
        <a href="./editprofile.jsp" class="button primary"
           style="color: white"> Edit </a>
    </div>
</div>
<div id="footer">
    <div class="bg-blue fg-white">
        <div class="container">
            <div class="align-center padding20 text-medium">
                LABORATORIUM | SISTEM INFORMASI ITS
            </div>
        </div>
    </div>
</div>
</body>
</html>