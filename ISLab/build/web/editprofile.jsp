<%-- 
    Document   : editprofile
    Created on : Dec 28, 2015, 4:49:43 AM
    Author     : fandi
--%>

<html>
    <head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Kerja Praktek Sistem Informasi ITS">
    <meta name="keywords" content="KP, Kerja Praktek, Sistem Informasi, ITS">
    <meta name="author" content="Hendra Rismana">

    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
    <title>Laboratorium Sistem Informasi ITS</title>

    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myForm").validate({
                rules: {
                    id : {
                        digits: true,
                        minlength:10,
                        maxlength:10
                    },
                }
            });
        });
    </script>

    </head>
    <body>
        <div>
            <div class="app-bar bg-blue fixed-top" data-role="appbar">
		<a class="app-bar-element" href="./">LABORATORIUM SI ITS</a>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./logout.jsp"><span class="mif-enter"></span> LogOut</a>
                </div>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="./profile2.jsp"><span class="mif-enter"></span> Profil</a>
                </div>
		<div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./contactus.jsp"> CONTACT US </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./peminjaman.jsp"> PEMINJAMAN </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./index.jsp"> HOME </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-content">
		<div class="formback padding20 block-shadow fg-blue" style="margin:20px 300px">
			<form id="myForm" method=post action=?menu=submiteditan>
				<h1 class="text-light">Edit Profil</h1>
				<hr class="thin"/>
				<br />
				<div class="input-control text" data-role="input" style="width: 59%">
					<label>NRP/NIP</label>
                                        <input type="text" name="nrpnip" maxlength="10" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Nama Lengkap</label>
					<input type="text" name="nama" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Jenis Kelamin</label>
					<select name="gender">
                                            <option value="male" selected="selected">Laki-laki </option>
                                            <option value="female">Perempuan</option>
                                        </select>
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Email</label>
					<input type="email" name="emaul" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Nomor Telepon/HP</label>
					<input type="text" name="phone" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Username</label>
					<input type="text" name="username" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input">
					<label>Password</label>
					<input type="password" name="password" class="required">
				</div>
                                <div class="input-control text" data-role="input">
					<label>Re-enter Password</label>
					<input type="password" name="repassword" class="required">
				</div>
				<br />
				<br />
                                <div class="input-control text" data-role="input" style="width: 59%">
					<label>Group</label>
					<select name="group">
                                            <option value="dosen" selected="selected">Dosen</option>
                                            <option value="karyawan">Karyawan</option>
                                            <option value="mahasiswa">Mahasiswa</option>
                                        </select>
				</div>
				<br />
				<br />
				<div class="form-actions">
					<button type="submit" class="button primary"> Update </button>
				</div>
			</form>
		</div>
	</div>
